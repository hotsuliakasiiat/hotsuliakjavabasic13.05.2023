package com.hotsuliakasiiat;

public class Rugby {
    public static void main(String[] args) {
        int[] ageTeamOne = {20, 21, 20, 25, 20, 20, 20, 20, 22, 20, 20, 30, 20, 20, 20, 20, 20, 20, 20, 30, 24, 20, 20, 20, 28};
        int[] ageTeamTwo = {25, 21, 20, 25, 20, 226, 20, 23, 22, 27, 20, 30, 25, 20, 20, 29, 21, 20, 29, 30, 24, 20, 20, 20, 28};
        System.out.println("Average age of Team One is " + countAverageAge(ageTeamOne));
        System.out.println("Average age of Team Two is " + countAverageAge(ageTeamTwo));
    }

    public static void ages(int[] ages) {
        for (int i = 0; i < ages.length; i++) {
            System.out.print(ages[i] + " ");
        }
        System.out.println();
    }

    public static double countAverageAge(int[] ages) {
        double sum = 0;
        for (int i = 0; i < ages.length; i++) {
            sum += ages[i];
        }
        return sum / ages.length;
    }
}
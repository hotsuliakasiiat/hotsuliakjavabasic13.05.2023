package com.hotsuliakasiiat;

public class Androids implements Smartphones, LinuxOS {
    @Override
    public void Info() {
        System.out.println("Linux OS on an Android device");
    }

    @Override
    public void call() {
        System.out.println("Call from Androids");
    }

    @Override
    public void sms() {
        System.out.println("Hello from Androids");

    }

    @Override
    public void internet() {
        System.out.println("Browsing the internet on an Android device");

    }
}

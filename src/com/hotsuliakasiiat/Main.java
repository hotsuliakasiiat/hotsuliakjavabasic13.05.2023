package com.hotsuliakasiiat;

public class Main {
    public static void main(String[] args) {
        Smartphones androids = new Androids();
        Smartphones iPhones = new iPhones();
        LinuxOS LinuxOS = new Androids();
        iOS iOS = new iPhones();

        LinuxOS.Info();
        androids.call();
        androids.sms();
        androids.internet();
        iOS.Info();
        iPhones.call();
        iPhones.sms();
        iPhones.internet();
    }
}

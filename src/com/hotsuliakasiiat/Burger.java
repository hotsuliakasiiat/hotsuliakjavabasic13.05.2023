package com.hotsuliakasiiat;

class Burger {
    private String bread;
    private String meat;
    private String meat2;
    private String cheese;
    private String salat;
    private String creme;

    public Burger(String bread, String meat, String cheese, String salat, String creme) {
        this.bread = bread;
        this.meat = meat;
        this.cheese = cheese;
        this.salat = salat;
        this.creme = creme;
        System.out.println("Classic burger: " + bread + " " + meat + " " + cheese + " " + salat + " " + creme);
    }

    public Burger(String bread, String meat, String cheese, String salat) {
        this.bread = bread;
        this.meat = meat;
        this.cheese = cheese;
        this.salat = salat;
        System.out.println("Ohne Creme: " + bread + " " + meat + " " + cheese + " " + salat);

    }

    public Burger(String bread, String meat, String meat2, String cheese, String salat, String creme) {
        this.bread = bread;
        this.meat = meat;
        this.meat2 = meat2;
        this.cheese = cheese;
        this.salat = salat;
        this.creme = creme;
        System.out.println("Double Meat: " + bread + " " + meat + " " + meat2 + " " + cheese + " " + salat + " " + creme);

    }

    public static void main(String[] args) {
        Burger classicBurger = new Burger("Bread", "Meat", "Cheese", "Salat", "Creme");
        Burger ohneCreme = new Burger("Bread", "Meat", "Cheese", "Salat");
        Burger doubleMeat = new Burger("Bread", "Meat", "Meat", "Cheese", "Salat", "Creme");
    }
}
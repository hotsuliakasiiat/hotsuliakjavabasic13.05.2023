package com.hotsuliakasiiat;

public class iPhones implements Smartphones, iOS {
    @Override
    public void Info() {
        System.out.println("iOS on an iPhone");
    }

    @Override
    public void call() {
        System.out.println("Call from iPhones");

    }

    @Override
    public void sms() {
        System.out.println("Hello from iPhones");

    }

    @Override
    public void internet() {
        System.out.println("Browsing the internet on an iPhone");

    }
}

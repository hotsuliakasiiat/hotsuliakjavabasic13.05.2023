package com.hotsuliakasiiat;

public class PersonInfo {
    String name;
    String surname;
    String city;
    String phoneNumber;

    public PersonInfo(String name, String surname, String city, String phoneNumber) {
        this.name = name;
        this.surname = surname;
        this.city = city;
        this.phoneNumber = phoneNumber;
    }


    public String personInfo() {
        return "You can reach out to " + name + " " + surname +
                " from " + city + " at the phone number " + phoneNumber;
    }

    public static void main(String[] args) {
        PersonInfo FirstPerson = new PersonInfo("Will", "Smith", "New York", "2936729462846");
        PersonInfo SecondPerson = new PersonInfo("Jackie", "Chan", "Shanghai", "12312412412");
        PersonInfo ThirdPerson = new PersonInfo("Sherlock", "Holmes", "London", "37742123513");

        System.out.println(FirstPerson.personInfo());
        System.out.println(SecondPerson.personInfo());
        System.out.println(ThirdPerson.personInfo());
    }
}

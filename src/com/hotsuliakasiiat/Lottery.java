package com.hotsuliakasiiat;

import java.util.Arrays;
import java.util.Random;

public class Lottery {
    public static void main(String[] args) {
        int[] sponsorsNumbers = generateNumbers(7);
        int[] playersNumbers = generateNumbers(7);
        Arrays.sort(sponsorsNumbers);
        Arrays.sort(playersNumbers);

        System.out.println(Arrays.toString(sponsorsNumbers));
        System.out.println(Arrays.toString(playersNumbers));

        int coincidences = numberMatches(sponsorsNumbers, playersNumbers);
        System.out.println("Number of matches: " + coincidences);
    }

    public static int[] generateNumbers(int length) {
        Random random = new Random();
        int[] array = new int[length];
        for (int i = 0; i < length; i++) {
            array[i] = random.nextInt(10);

        }
        return array;
    }

    public static int numberMatches(int[] array1, int[] array2) {
        int matches = 0;
        int length = Math.min(array1.length, array2.length);

        for (int i = 0; i < length; i++) {
            if (array1[i] == array2[i]) {
                matches++;
            }
        }
        return matches;
    }
}
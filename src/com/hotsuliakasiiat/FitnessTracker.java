package com.hotsuliakasiiat;

public class FitnessTracker {
    private final String name;
    private String surname;
    private final int birthYear;
    private final int birthMonth;
    private final int birthDay;
    private final String emeil;
    private final int number;
    private int weight;
    private int pressuret;
    private int stepCounter;
    private int age;


    public FitnessTracker(String name, String surname, int birthYear, int birthMonth, int birthDay, String emeil, int number, int weight, int pressuret, int stepCounter) {
        this.name = name;
        this.surname = surname;
        this.birthYear = birthYear;
        this.birthMonth = birthMonth;
        this.birthDay = birthDay;
        this.emeil = emeil;
        this.number = number;
        this.weight = weight;
        this.pressuret = pressuret;
        this.stepCounter = stepCounter;
    }

    public String getName() {
        return name;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public int getBirthMonth() {
        return birthMonth;
    }

    public int getBirthDay() {
        return birthDay;

    }

    public String getEmeil() {
        return emeil;
    }

    public int getNumber() {
        return number;
    }

    public int getAge() {
        age = 2023 - birthYear;
        return age;
    }

    public void printAccountInfo() {
        System.out.println("Name: " + getName());
        System.out.println("Surname: " + surname);
        System.out.println("Birth date: " + getBirthYear() + "-" + getBirthMonth() + "-" + getBirthDay());
        System.out.println("E-mail: " + getEmeil());
        System.out.println("Number: " + getNumber());
        System.out.println("Weight: " + weight);
        System.out.println("Pressuret: " + pressuret);
        System.out.println("Steps: " + stepCounter);
        System.out.println("Age: " + getAge());
    }

    public static void main(String[] args) {
        FitnessTracker firstUser = new FitnessTracker("Will", "Smith", 1996, 1, 1, "tS9I2@example.com", 123456789, 70, 80, 1000);
        FitnessTracker secondUser = new FitnessTracker("Jackie", "Chan", 1991, 1, 1, "tS9I2@example.com", 123456789, 70, 80, 1000);
        FitnessTracker thirdUser = new FitnessTracker("Sherlock", "Holmes", 2000, 1, 1, "tS9I2@example.com", 123456789, 70, 80, 1000);
        FitnessTracker fourthUser = new FitnessTracker("John", "Doe", 2004, 1, 1, "tS9I2@example.com", 123456789, 70, 80, 1000);
        FitnessTracker fifthUser = new FitnessTracker("Jane", "Doe", 2002, 1, 1, "tS9I2@example.com", 123456789, 70, 80, 1000);
        firstUser.printAccountInfo();
        secondUser.printAccountInfo();
        thirdUser.printAccountInfo();
        fourthUser.printAccountInfo();
        fifthUser.printAccountInfo();
        firstUser = new FitnessTracker("Will", "Miller", 1996, 1, 1, "tS9I2@example.com", 123456789, 70, 80, 1000);
        secondUser = new FitnessTracker("Jackie", "Smith", 1991, 1, 1, "tS9I2@example.com", 123456789, 70, 80, 1000);
        firstUser.printAccountInfo();
        secondUser.printAccountInfo();
    }
}
